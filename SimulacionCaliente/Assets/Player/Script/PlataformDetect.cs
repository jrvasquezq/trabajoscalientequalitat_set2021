﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformDetect : MonoBehaviour
{
    public GameObject playerWeb;
    public GameObject playerVR;
    private void Awake()
    {
#if UNITY_ANDROID
        playerWeb.SetActive(false);
        playerVR.SetActive(true);
#endif
#if UNITY_WEBGL
        playerWeb.SetActive(true);
        playerVR.SetActive(false);
#endif
    }
}
