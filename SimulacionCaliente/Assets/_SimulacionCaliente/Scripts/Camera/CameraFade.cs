using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFade : MonoBehaviour
{
    [SerializeField] Animator fade;
    
    /// <summary>
    /// Fades in screen
    /// </summary>
    public void FadeIn() {
        fade.SetBool("fadeOut", false);
        fade.SetBool("fadeIn", true);
        
    }

    /// <summary>
    /// Fades out screen
    /// </summary>
    public void FadeOut() {
        fade.SetBool("fadeIn", false);
        fade.SetBool("fadeOut", true);        
    }
}
