using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoaderManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetString("hand", "right");
        Invoke("LoadNextScene", 2.0f);
    }

    // Update is called once per frame
    void LoadNextScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
    }
}
