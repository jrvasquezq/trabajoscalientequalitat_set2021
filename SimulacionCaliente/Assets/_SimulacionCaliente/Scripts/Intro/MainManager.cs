using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainManager : MonoBehaviour
{
    [Header("Oculus")]
    [SerializeField] TMP_InputField documentVR;
    [SerializeField] TMP_InputField nameVR;
    [SerializeField] GameObject panelHand;
    [SerializeField] GameObject panelUserVR;

    [Header("Hands")]
    [SerializeField] GameObject leftHandController;
    [SerializeField] GameObject rightHandController;

    [Header("Keyboard")]
    [SerializeField] GameObject numericKeyboard;
    [SerializeField] GameObject alphaKeyboard;

    [Header("WebGL")]
    [SerializeField] GameObject panelUserWebGL;
    [SerializeField] GameObject panelStartWebGL;
    [SerializeField] TMP_InputField documentWebGL;
    [SerializeField] TMP_InputField nameWebGL;

    GameObject panelUser;
    TMP_InputField document;
    TMP_InputField name;

    private void Start()
    {
        SelectPanels();

#if UNITY_ANDROID
        ChangeDominantHand("");
#endif
    }

    /// <summary>
    /// Selects panel according to platform
    /// </summary>
    private void SelectPanels()
    {
#if UNITY_ANDROID
        panelUser = panelUserVR;

        document = documentVR;
        name = nameVR;

        ActivatePanelHand();

#elif UNITY_WEBGL
        panelUser = panelUserWebGL;

        document = documentWebGL;
        name = nameWebGL;

#endif
    }

    /// <summary>
    /// Changes current dominant hand
    /// </summary>
    /// <param name="hand"></param>
    public void ChangeDominantHand(string hand) {

        switch (hand) {
            case "izq":
                rightHandController.SetActive(false);
                leftHandController.SetActive(true);
                PlayerPrefs.SetString("hand", "left");
                ActivatePanelUser();
                break;

            case "der":
                leftHandController.SetActive(false);
                rightHandController.SetActive(true);
                PlayerPrefs.SetString("hand", "right");
                ActivatePanelUser();
                break;

            case "":
                string curHand = PlayerPrefs.GetString("hand", "right");
                if (curHand == "right"){
                    leftHandController.SetActive(false);
                    rightHandController.SetActive(true);
                    PlayerPrefs.SetString("hand", "right");
                }
                else if (curHand == "left") {
                    rightHandController.SetActive(false);
                    leftHandController.SetActive(true);
                    PlayerPrefs.SetString("hand", "left");
                }
                break;
        }        
    }

    public void DeactivateStartPanel() {
        panelStartWebGL.SetActive(false);
        panelUser.SetActive(true);
    }

    public void ActivatePanelHand() {
        Debug.Log("Activating Hand panel");
        panelUser.SetActive(false);
        panelHand.SetActive(true);
    }

    public void ActivatePanelUser() {
        Debug.Log("Activating user panel");
        panelHand.SetActive(false);
        panelUser.SetActive(true);
    }

    public void ActivateNumericKeyboard(bool activate) {
        numericKeyboard.SetActive(activate);
    }

    public void ActivateAlphaKeyboard(bool activate) {
        alphaKeyboard.SetActive(activate);
    }

    /// <summary>
    /// Validates user input
    /// </summary>
    public void ValidateDocument()
    {
        string userID = document.text;
        string userName = name.text;

        if (userID == "")
        {
            userID = "000000000";
        }
        else
        {
            if (userName == "")
                userName = "Invitado";

            FindObjectOfType<Data>().AddUserId(userID, userName);
            StartCoroutine(GoToNext());
        }
    }

    /// <summary>
    /// Loads next scene
    /// </summary>
    IEnumerator GoToNext() {

#if UNITY_ANDROID
        leftHandController.SetActive(false);
        rightHandController.SetActive(false);
#endif
        FindObjectOfType<CameraFade>().FadeOut();

        yield return new WaitForSeconds(4.0f);

#if UNITY_ANDROID
        SceneManager.LoadScene("Tutorial");
#elif UNITY_WEBGL
        SceneManager.LoadScene("Tutorial");
#endif
    }

    /// <summary>
    /// Quits simulation
    /// </summary>
    public void Quit() {
        Application.Quit();
    }

}
