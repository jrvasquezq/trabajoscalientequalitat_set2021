using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{    
    [Header("SFX")]
    [SerializeField] AudioClip buttonPressed;
    [SerializeField] AudioClip welding;
    [SerializeField] AudioSource aSource;

    //[Header("Voice over")]

    [Header("Voice over")]
    [SerializeField] AudioSource voiceSource;

    [Header("BGM")]
    [SerializeField] AudioClip bgm;
    [SerializeField] AudioClip bgmTutorial;
    [SerializeField] AudioSource bgmSource;

    [Header("Variables")]
    [SerializeField] float maxVolTutorial;
    [SerializeField] float maxVolSimulation;
    [SerializeField] float volMultiplier;
    [SerializeField] float duration;
    [SerializeField] float durationEnd;


    /// <summary>
    /// Play button pressed clip
    /// </summary>
    public void PlayButtonPressed() {
        aSource.clip = buttonPressed;
        aSource.loop = false;

        aSource.Stop();
        aSource.Play();
    }

    /// <summary>
    /// Plays background sound
    /// </summary>
    /// <param name="state">True to play, false otherwise</param>
    public void PlayBGM(bool state) {

        if (state)
        {
            bgmSource.Stop();

            bgmSource.clip = bgm;
            bgmSource.volume = 0.0f;

            bgmSource.Play();
            StartCoroutine(FadeInSource(bgmSource, maxVolSimulation));
        }
        else
        {
            StartCoroutine(FadeOutSource(bgmSource, 0.0f));
        }
    }

    public void PlayBGMTutorial(bool state) {       

        if (state)
        {
            bgmSource.Stop();

            bgmSource.clip = bgmTutorial;
            bgmSource.volume = 0.0f;

            bgmSource.Play();
            StartCoroutine(FadeInSource(bgmSource, maxVolTutorial));
        }
        else
        {
            StartCoroutine(FadeOutSource(bgmSource, 0.0f));
        }
    }

    IEnumerator FadeInSource(AudioSource source, float targetVolume) {

        float volume = 0.0f;
        float start = source.volume;

        while (source.volume < targetVolume) {
            volume += volMultiplier;
            source.volume = Mathf.Lerp(start, targetVolume, volume / duration);

            yield return null;
        }

        yield return new WaitForSeconds(0.1f);
    }

    IEnumerator FadeOutSource(AudioSource source, float targetVolume)
    {
        float volume = source.volume;
        float start = source.volume;

        while (source.volume > targetVolume) {

            volume -= volMultiplier;            
            source.volume = Mathf.Lerp(start, targetVolume, -volume / durationEnd);

            yield return null;
        }

        yield return new WaitForSeconds(0.01f);
    }

    /// <summary>
    /// Plays welding sound
    /// </summary>
    public void PlayWeldingSound() {
        aSource.clip = welding;
        aSource.loop = true;

        aSource.Stop();
        aSource.Play();
    }

    /// <summary>
    /// Stops sound effects
    /// </summary>
    public void StopSFX() {
        aSource.loop = false;
        aSource.Stop();
    }

    public void PlayQ1() { 
        
    }

    public void PlayQ2(){

    }

    public void PlayQ3(){

    }

    public void PlayQ4(){

    }

    public void PlayQ5()
    {

    }

}
