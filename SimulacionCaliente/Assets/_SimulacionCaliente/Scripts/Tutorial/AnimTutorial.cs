using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimTutorial : MonoBehaviour
{
    public void ChangeLesson() {
#if UNITY_ANDROID
        FindObjectOfType<TutorialVR>().GoToNextLesson();
#elif UNITY_WEBGL
        FindObjectOfType<TutorialWeb>().GoToNextLesson();
#endif
    }

    public void DisplayLesson()
    {
#if UNITY_ANDROID
        FindObjectOfType<TutorialVR>().DisplayLesson();
#elif UNITY_WEBGL
        FindObjectOfType<TutorialWeb>().DisplayLesson();
#endif
    }
}
