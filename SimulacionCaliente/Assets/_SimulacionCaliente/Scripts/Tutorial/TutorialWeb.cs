using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class TutorialWeb : MonoBehaviour
{
    [SerializeField] Animator tutorialPanel;

    [Header("Lessons")]
    [SerializeField] GameObject[] lessons;
    [SerializeField] GameObject lesson4a;
    [SerializeField] GameObject lesson5a;

    [Header("Lesson items")]
    [SerializeField] GameObject[] indicators;

    [Header("Variables")]
    [SerializeField] int currentLesson;

    GameObject[] targetTicks;

    int counter;
    int lessonAmount;
    

    /*STAGE VARIABLES*/
    float horizontal;
    float vertical;
    bool stageComplete;
    bool runUpdate;

    public int Counter { get => counter; set => counter = value; }

    // Start is called before the first frame update
    void Start()
    {
        lessonAmount = lessons.Length;
        FindObjectOfType<WebGLInteractions>().TeleportEnabled = false;

        FindObjectOfType<AudioManager>().PlayBGMTutorial(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (runUpdate)
            CheckStageCompletion();
    }

    public void DisplayLesson() {
        Debug.LogFormat("Displaying lesson, counter at: {0}", Counter);

        lessons[currentLesson].SetActive(true);
        targetTicks = GameObject.FindGameObjectsWithTag("ticks");

        foreach (GameObject go in targetTicks)
        {
            go.SetActive(false);
        }

        if (currentLesson == 2)//Teleport
            FindObjectOfType<WebGLInteractions>().TeleportEnabled = true;

        if (currentLesson == 3)//Indicator
        {
            foreach (GameObject go in indicators)
                go.SetActive(true);
        }

        if (currentLesson == lessonAmount - 1) {//Final
            FindObjectOfType<AudioManager>().PlayBGMTutorial(false);
        }

        runUpdate = true;
    }

    public void GoToNextLesson() {
        Debug.Log("Showing next lesson");

        lessons[currentLesson].SetActive(false);

        currentLesson++;
        stageComplete = false;

        if (currentLesson < lessonAmount)
        {
            tutorialPanel.SetBool("out", false);
            tutorialPanel.SetBool("in", true);
        }
    }

    #region Stages
    void CheckStageCompletion()
    {
        //Movement
        if (currentLesson == 0 && !stageComplete)
        {
            if (Input.GetAxisRaw("Horizontal") > 0 || Input.GetAxisRaw("Horizontal") < 0)
                horizontal = Input.GetAxisRaw("Horizontal");
            if (Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Vertical") < 0)
                vertical = Input.GetAxisRaw("Vertical");

            if (horizontal != 0 && vertical != 0)
            {
                targetTicks[Counter].SetActive(true);
                Counter++;
            }

            if (Counter == targetTicks.Length)
            {
                stageComplete = true;
                Counter = 0;
                tutorialPanel.SetBool("in", false);
                tutorialPanel.SetBool("out", true);
            }

        }

        //Zoom
        if (currentLesson == 1 && !stageComplete)
        {
            if (Input.GetMouseButtonUp(1))
            {
                targetTicks[Counter].SetActive(true);
                Counter++;
            }

            if (Counter == targetTicks.Length)
            {
                stageComplete = true;
                Counter = 0;
                tutorialPanel.SetBool("in", false);
                tutorialPanel.SetBool("out", true);
            }
        }

        //Teleport
        if (currentLesson == 2 && !stageComplete)
        {
            if (Counter == targetTicks.Length)
            {
                FindObjectOfType<WebGLInteractions>().TeleportEnabled = false;
                stageComplete = true;
                Counter = 0;                
                StartCoroutine(TeleportToArrow());                
            }
        }

        //Walking to indicator
        if (currentLesson == 3 && !stageComplete)
        {
            if (Counter == targetTicks.Length)
            {
                foreach (GameObject go in indicators)
                    go.SetActive(false);

                stageComplete = true;
                Counter = 0;

                FindObjectOfType<RigidbodyFirstPersonController>().mouseLook.lockCursor = true;

                tutorialPanel.SetBool("in", false);
                tutorialPanel.SetBool("out", true);
            }
        }

        //Spacebar
        if (currentLesson == 4 && !stageComplete)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (Counter == 0)
                {
                    targetTicks[Counter].SetActive(true);
                    Counter++;                    
                }

                if (!lesson5a.activeInHierarchy)
                {
                    FindObjectOfType<RigidbodyFirstPersonController>().mouseLook.lockCursor = false;

                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;

                    lesson5a.SetActive(true);
                }
                else if (lesson5a.activeInHierarchy)
                {
                    FindObjectOfType<RigidbodyFirstPersonController>().mouseLook.lockCursor = true;

                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;

                    lesson5a.SetActive(false);
                }
            }

            if (Counter == targetTicks.Length)
            {
                stageComplete = true;
                Counter = 0;
                tutorialPanel.SetBool("in", false);
                tutorialPanel.SetBool("out", true);
            }
        }
    }

    /// <summary>
    /// Increases current counter after teleporting
    /// </summary>
    public void IncreaseTeleportCounter() {
        targetTicks[Counter].SetActive(true);
        Counter++;
    }

    IEnumerator TeleportToArrow()
    {
        yield return new WaitForSeconds(3.0f);

        tutorialPanel.SetBool("in", false);
        tutorialPanel.SetBool("out", true);
    }

    /// <summary>
    /// Increases current counter after entering an arrow
    /// </summary>
    public void IncreaseArrowCounter(GameObject arrow) {
        targetTicks[Counter].SetActive(true);
        Counter++;

        arrow.SetActive(false);
        lesson4a.SetActive(true);

        FindObjectOfType<RigidbodyFirstPersonController>().mouseLook.lockCursor = false;
        
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    /// <summary>
    /// Generic. Used to increase counter when a button is clicked during tutorial
    /// </summary>
    /// <param name="go">GameObject to be deactivated</param>
    public void IncreaseCounter(GameObject go) {
        targetTicks[Counter].SetActive(true);
        Counter++;

        go.SetActive(false);
    }

    #endregion
}
