using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialVR : MonoBehaviour
{
    [Header("Lessons")]
    [SerializeField] GameObject[] lessons;

    [Header("Variables")]
    [SerializeField] int currentLesson;
    [SerializeField] CameraSwitch cameraSwitch;

    [Header("Tablet elements")]
    [SerializeField] BoxCollider[] questionColliders;
    [SerializeField] BoxCollider[] locationColliders;
    [SerializeField] GameObject location;
    [SerializeField] GameObject question;
    [SerializeField] Transform tablet;
    [SerializeField] GameObject map;
    [SerializeField] GameObject info;

    [Header("Hands")]
    [SerializeField] GameObject rightHandPointer;
    [SerializeField] Transform leftHand;
    [SerializeField] GameObject leftHandPointer;
    [SerializeField] Transform rightHand;
    string hand;

    int lessonAmount;
    int counter;

    bool stageComplete;

    bool runUpdate;

    GameObject[] targetTicks;

    public int Counter { get => counter; set => counter = value; }

    // Start is called before the first frame update
    void Start()
    {
        lessonAmount = lessons.Length;
        hand = PlayerPrefs.GetString("hand");

        FindObjectOfType<LocomotionController>().InitializeLocomotionController(hand);
    }

    public void GoToNextLesson()
    {
        lessons[currentLesson].SetActive(false);

        currentLesson++;
        stageComplete = false;

        if (currentLesson != 3)
        {
            cameraSwitch.ChangeCurrentLocation(0);

            Invoke("DisplayLesson", 4.0f);
        }
        else if (currentLesson == 3)
        {
            DisplayLesson();
        }
    }

    public void SkipTutorial() {
        foreach (GameObject go in lessons)
            go.SetActive(false);

        Counter = 0;

        EnableColliders(questionColliders, false);
        EnableColliders(locationColliders, false);

        currentLesson = 3;

        info.SetActive(false);

        DisplayLesson();
    }

    public void DisplayLesson() {
        Debug.LogFormat("Displaying lesson");

        info.SetActive(false);

        lessons[currentLesson].SetActive(true);
        targetTicks = GameObject.FindGameObjectsWithTag("ticks");

        foreach (GameObject go in targetTicks)
        {
            go.SetActive(false);
        }
        
        runUpdate = true;

        if (currentLesson == 0) {
            if (hand == "right")
                GameObject.Find("cLeft").SetActive(false);
            else if (hand == "left")
                GameObject.Find("cRight").SetActive(false);

            EnableColliders(questionColliders, false);
            EnableColliders(locationColliders, false);
        }

        if (currentLesson == 1) {
            tablet.gameObject.SetActive(true);

            SelectHands();

            EnableColliders(questionColliders, false);
            EnableColliders(locationColliders, true);
        }

        if (currentLesson == 2)
        {
            EnableColliders(questionColliders, true);
            EnableColliders(locationColliders, false);
            ActivateMap(false);
        }

        if (currentLesson == 3)
            StartCoroutine(GoToSimulation());
    }

    // Update is called once per frame
    void Update()
    {
        if (runUpdate)
            CheckStageCompletion();
    }

    #region Stages
    public int GetCurrentLesson() {
        return currentLesson;
    }

    void CheckStageCompletion()
    {
        //Teleport controller
        if (currentLesson == 0 && !stageComplete)
        {
            if (Counter == targetTicks.Length)
            {
                stageComplete = true;
                Counter = 0;

                info.SetActive(true);

                GoToNextLesson();
            }
        }

        //Teleport tablet
        if (currentLesson == 1 && !stageComplete)
        {
            if (Counter == targetTicks.Length) {

                EnableColliders(questionColliders, false);
                EnableColliders(locationColliders, false);

                stageComplete = true;
                Counter = 0;

                info.SetActive(true);

                GoToNextLesson();
            }
        }

        //Questions
        if (currentLesson == 2 && !stageComplete)
        {
            if (Counter == targetTicks.Length)
            {
                stageComplete = true;
                Counter = 0;
                GoToNextLesson();
            }
        }

    }

    /// <summary>
    /// Increases current stage counter
    /// </summary>
    public void IncreaseCounterVRTutorial() {
        try
        {
            targetTicks[Counter].SetActive(true);
        }
        catch (Exception ex) {
            Debug.Log(ex.Message);
        }
        Counter++;        
    }

    /// <summary>
    /// Enables/disables colliders
    /// </summary>
    /// <param name="colliders">Collider array</param>
    /// <param name="enable">True to enable, false otherwise</param>
    void EnableColliders(BoxCollider[] colliders, bool enable) {
        foreach (BoxCollider collider in colliders)
        {
            collider.enabled = enable;
        }
    }

    /// <summary>
    /// Disables question colliders
    /// </summary>
    public void DisableQuestionColliders() {
        EnableColliders(questionColliders, false);
    }

    /// <summary>
    /// Waits for a fixed amount of seconds before changing scene
    /// </summary>
    /// <returns></returns>
    IEnumerator GoToSimulation() {

        EnableColliders(questionColliders, false);
        EnableColliders(locationColliders, false);

        yield return new WaitForSeconds(2.0f);
        tablet.gameObject.SetActive(false);

        yield return new WaitForSeconds(3.0f);

        cameraSwitch.GoToTargetScene("IntroIES");
    }

    #endregion

    #region Interactions
    void SelectHands()
    {
        string hand = PlayerPrefs.GetString("hand");

        if (hand == "right")
        {
            rightHandPointer.SetActive(true);
            leftHand.gameObject.SetActive(true);

            leftHandPointer.SetActive(false);
            rightHand.gameObject.SetActive(false);

            tablet.parent = leftHand;

            Transform tabPos = GameObject.Find("tabletPosition").transform;
            tablet.localPosition = tabPos.localPosition;
            tablet.localRotation = tabPos.localRotation;
            tablet.localScale = tabPos.localScale;

            tablet.gameObject.SetActive(true);

        }
        else if (hand == "left")
        {
            leftHandPointer.SetActive(true);
            rightHand.gameObject.SetActive(true);

            rightHandPointer.SetActive(false);
            leftHand.gameObject.SetActive(false);

            tablet.parent = rightHand;

            Transform tabPos = GameObject.Find("tabletPosition").transform;
            tablet.localPosition = tabPos.localPosition;
            tablet.localRotation = tabPos.localRotation;
            tablet.localScale = tabPos.localScale;

            tablet.gameObject.SetActive(true);

        }
    }

    public void ActivateMap(bool activate) {
        map.SetActive(activate);
        question.SetActive(!activate);
    }
    #endregion
}
