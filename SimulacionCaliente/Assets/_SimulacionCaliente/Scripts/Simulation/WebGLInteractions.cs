using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class WebGLInteractions : MonoBehaviour
{
    [Header("Camera")]
    [SerializeField] Camera webglCamera;    
    [SerializeField] float speed;
    [SerializeField] float mouseSpeedX;
    [SerializeField] float mouseSpeedY;
    [SerializeField] bool canCameraBeMoved;
    [SerializeField] bool canCameraBeZoomed;
    float maxFov;
    float minFov;
    float yaw;
    float pitch;

    [Header("Walk")]
    [SerializeField] float walkSpeed;
    [SerializeField] Transform movableObject;

    [Header("Variables")]
    [SerializeField] Questions questions;

    [Header("Panels")]    
    [SerializeField] GameObject helpPanel;

    [Header("Interaction arrows")]
    [SerializeField] GameObject[] interactionArrows;

    bool isActive;
    bool panelActive;

    public bool CanCameraBeMoved { get => canCameraBeMoved; set => canCameraBeMoved = value; }
    public bool CanCameraBeZoomed { get => canCameraBeZoomed; set => canCameraBeZoomed = value; }
    public bool TeleportEnabled { get => teleportEnabled; set => teleportEnabled = value; }

    CameraSwitch camSwitch;
    bool teleportEnabled;

    // Start is called before the first frame update
    void Start() {
        maxFov = 60.0f;
        minFov = 20.0f;
        yaw = 0.0f;
        pitch = 0.0f;

        camSwitch = FindObjectOfType<CameraSwitch>();
        teleportEnabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        Zoom();
        DisplayQuestion();
        Teleport();
        DisplayHelp();
    }

    /// <summary>
    /// Displays help when tab key is pressed
    /// </summary>
    void DisplayHelp() {

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (!panelActive)
            {
                panelActive = true;
                ActivateHelpPanel(true);
            }
            else if (panelActive) {
                panelActive = false;
                ActivateHelpPanel(false);
            }
        }
    }

    /// <summary>
    /// Activates/deactivates help panel
    /// </summary>
    /// <param name="activate">True to activate, false otherwise</param>
    public void ActivateHelpPanel(bool activate) {
        helpPanel.SetActive(activate);

        if (!activate)
        {
            FindObjectOfType<RigidbodyFirstPersonController>().mouseLook.lockCursor = true;

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (activate)
        {
            FindObjectOfType<RigidbodyFirstPersonController>().mouseLook.lockCursor = false;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        
    }

    void Teleport()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            teleportEnabled = false;
            camSwitch.ChangeCurrentLocation(1);
        }
    }

    public void DisplayQuestion()
    {
        if (SceneManager.GetActiveScene().name != "Tutorial")
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (!isActive)
                {
                    questions.DisplayCurrentQuestion(true);
                    isActive = true;
                }
                else if (isActive)
                {
                    questions.DisplayCurrentQuestion(false);
                    isActive = false;
                }
            }
        }
    }

    void Zoom() {
        if (CanCameraBeZoomed)
        {
            if (Input.GetMouseButton(1))
            {
                ZoomIn();
            }
            else
            {
                ZoomOut();
            }
        }
    }

    void ZoomIn() {

        float fov = webglCamera.fieldOfView - speed;

        if (fov > minFov)
            webglCamera.fieldOfView = fov;
    }

    void ZoomOut() {
        float fov = webglCamera.fieldOfView + speed;

        if (fov <= maxFov)
            webglCamera.fieldOfView = fov;
    }


    public void ResetCamera() {
        CanCameraBeMoved = false;
        webglCamera.transform.eulerAngles = Vector3.zero;

        Cursor.lockState = CursorLockMode.None;
    }

    /// <summary>
    /// Get interaction arrows
    /// </summary>
    /// <returns>Array with all interaction arrows</returns>
    public GameObject[] GetInteractionArrows() {
        return interactionArrows;
    }
}
