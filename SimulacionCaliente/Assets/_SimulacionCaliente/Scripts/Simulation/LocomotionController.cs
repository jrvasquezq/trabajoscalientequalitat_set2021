using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class LocomotionController : MonoBehaviour
{
    [Header("Locomotion variables")]
    [SerializeField] InputActionAsset actionAsset;
    [SerializeField] XRRayInteractor rayInteractorLeft;
    [SerializeField] XRRayInteractor rayInteractorRight;
    [SerializeField] TeleportationProvider provider;

    [Header("Debug")]
    [SerializeField] bool isDebug;

    XRRayInteractor rayInteractor;
    InputAction thumbstick;
    bool isActive;
    bool canPerformUpdates;
    bool isSkipped;

    private void Start()
    {
        if (isDebug)
            InitializeLocomotionController("right");
    }
    
    /// <summary>
    /// Initializes locomotion controller after checking dominant hand
    /// </summary>
    public void InitializeLocomotionController(string hand) {

        string xriHand = "";

        //Enabling interactor
        switch (hand) {
            case "right":
                rayInteractor = rayInteractorRight;
                xriHand = "XRI RightHand";

                rayInteractorLeft.enabled = false;
                rayInteractorLeft.gameObject.GetComponent<LineRenderer>().enabled = false;
                rayInteractorLeft.gameObject.GetComponent<XRInteractorLineVisual>().enabled = false;
                break;

            case "left":
                rayInteractor = rayInteractorLeft;
                xriHand = "XRI LeftHand";

                rayInteractorRight.enabled = false;
                rayInteractorRight.gameObject.GetComponent<LineRenderer>().enabled = false;
                rayInteractorRight.gameObject.GetComponent<XRInteractorLineVisual>().enabled = false;
                break;
        }

        //Activating variables
        rayInteractor.enabled = false;

        var activate = actionAsset.FindActionMap(xriHand).FindAction("Teleport Mode Activate");
        activate.Enable();
        activate.performed += OnTeleportActivate;

        var cancel = actionAsset.FindActionMap(xriHand).FindAction("Teleport Mode Cancel");
        cancel.Enable();
        cancel.performed += OnTeleportCancel;

        var skip = actionAsset.FindActionMap(xriHand).FindAction("Skip");
        skip.Enable();
        skip.performed += OnSkipActivate;

        thumbstick = actionAsset.FindActionMap(xriHand).FindAction("Move");
        thumbstick.Enable();
        canPerformUpdates = true;
    }

    private void OnSkipActivate(InputAction.CallbackContext obj)
    {
        if (!isSkipped) {
            if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Tutorial") {
                isSkipped = true;
                FindObjectOfType<TutorialVR>().SkipTutorial();
            }
            
        }
    }

    private void OnTeleportCancel(InputAction.CallbackContext obj)
    {
        rayInteractor.enabled = false;
        isActive = false;
    }

    private void OnTeleportActivate(InputAction.CallbackContext obj)
    {
        rayInteractor.enabled = true;
        isActive = true;
    }

    // Start is called before the first frame update
    void Update()
    {
        if (canPerformUpdates)
        {
            if (!isActive)
                return;

            //if (thumbstick.triggered)
            if (thumbstick.ReadValue<Vector2>() != Vector2.zero)
                return;

            if (!rayInteractor.TryGetCurrent3DRaycastHit(out RaycastHit hit))
            {

                rayInteractor.enabled = false;
                isActive = false;

                return;
            }
            else
            {
                TeleportRequest teleportRequest = new TeleportRequest()
                {
                    destinationPosition = hit.point
                };

                provider.QueueTeleportRequest(teleportRequest);

                if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Tutorial" && FindObjectOfType<TutorialVR>().GetCurrentLesson() == 0)
                    FindObjectOfType<TutorialVR>().IncreaseCounterVRTutorial();

                rayInteractor.enabled = false;
                isActive = false;
                rayInteractor.GetComponent<XRInteractorLineVisual>().reticle.SetActive(false);
                return;

            }            
        }
    }

}
