using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Questions : MonoBehaviour
{
    [Header("Questions Characters")]
    [SerializeField] GameObject ppeChar;
    [SerializeField] GameObject welderChar;
    [SerializeField] GameObject welderDown;
    [SerializeField] GameObject welderAsistant;

    [Header("Questions papers")]
    [SerializeField] GameObject[] papers;

    [Header("Hands")]
    [SerializeField] GameObject leftHandPointer;
    [SerializeField] GameObject rightHandPointer;
    [SerializeField] Transform leftHand;
    [SerializeField] Transform rightHand;

    [Header("Tablet")]
    [SerializeField] Transform tablet;
    [SerializeField] GameObject sharedItems;
    [SerializeField] GameObject tabletScreensaver;
    [SerializeField] GameObject tabletFinish;
    [SerializeField] GameObject map;

    [Header("Questions Oculus")]
    [SerializeField] GameObject[] questionsOculus;

    [Header("Questions WebGL")]
    [SerializeField] GameObject[] questionsWebGL;
    GameObject[] interactionArrows;
    WebGLInteractions interactions;

    GameObject[] questions;
    Data data;
    AudioManager audioManager;
    CameraSwitch cameraSwitch;

    int correctCount;
    int wrongCount;
    int currentQuestion; 

    void Start() {

        data = FindObjectOfType<Data>();
        audioManager = FindObjectOfType<AudioManager>();

        audioManager.PlayBGM(true);

        cameraSwitch = GetComponent<CameraSwitch>();
#if UNITY_ANDROID
        SelectHands();
#endif
#if UNITY_WEBGL
        interactions = FindObjectOfType<WebGLInteractions>();
        interactionArrows = interactions.GetInteractionArrows();
#endif
        SelectQuestionArray();
    }

    /// <summary>
    /// Selects current hand
    /// </summary>
    void SelectHands()
    {
        string hand = PlayerPrefs.GetString("hand");

        if (hand == "right")
        {
            rightHandPointer.SetActive(true);
            leftHand.gameObject.SetActive(true);

            leftHandPointer.SetActive(false);
            rightHand.gameObject.SetActive(false);

            tablet.parent = leftHand;

            Transform tabPos = GameObject.Find("tabletPosition").transform;
            tablet.localPosition = tabPos.localPosition;
            tablet.localRotation = tabPos.localRotation;
            tablet.localScale = tabPos.localScale;

            tablet.gameObject.SetActive(true);
        }
        else if (hand == "left")
        {
            leftHandPointer.SetActive(true);
            rightHand.gameObject.SetActive(true);

            rightHandPointer.SetActive(false);
            leftHand.gameObject.SetActive(false);

            tablet.parent = rightHand;

            Transform tabPos = GameObject.Find("tabletPosition").transform;
            tablet.localPosition = tabPos.localPosition;
            tablet.localRotation = tabPos.localRotation;
            tablet.localScale = tabPos.localScale;

            tablet.gameObject.SetActive(true);

        }

        FindObjectOfType<LocomotionController>().InitializeLocomotionController(hand);
    }

    /// <summary>
    /// Selects question array according to the OS
    /// </summary>
    void SelectQuestionArray() {
#if UNITY_ANDROID
        questions = questionsOculus;
#endif
#if UNITY_WEBGL
        questions = questionsWebGL;
#endif
    }

    /// <summary>
    /// Activates question panels and characters to be displayed
    /// </summary>
    /// <param name="questionIndex">Current question index</param>
    public void ActivateQuestion(int questionIndex) {

        DeactivateAllCharacters();
        DeactivateAllQuestions();

#if UNITY_ANDROID
        EnableSharedItems(false);        
        tabletScreensaver.SetActive(true);

        FindObjectOfType<HandCollision>().ResetSimulationVariables();
#endif

        switch (questionIndex) {

            case 1:
                ppeChar.SetActive(true);

                foreach (GameObject go in papers)
                    go.SetActive(false);

                break;

            case 2: 
                ppeChar.SetActive(false);
                welderChar.SetActive(true);
                break;

            case 4: 
                welderChar.SetActive(true);
                welderDown.SetActive(true);
                break;

            case 5:
                welderChar.SetActive(true);
                welderDown.SetActive(true);
                welderAsistant.SetActive(true);
                audioManager.PlayWeldingSound();
                break;
        }

        currentQuestion = questionIndex;

#if UNITY_WEBGL
        foreach (GameObject go in interactionArrows)
            go.SetActive(false);

        interactionArrows[currentQuestion].SetActive(true);
#endif

#if UNITY_ANDROID
        StartCoroutine(ActivateQuestionAfterDelay(questionIndex));
#endif
    }

    /// <summary>
    /// Activates question panels after delay
    /// </summary>
    /// <param name="questionIndex">Current index to activate</param>
    /// <returns></returns>
    IEnumerator ActivateQuestionAfterDelay(int questionIndex) {

        yield return new WaitForSeconds(6.0f);

        EnableSharedItems(true);
        tabletScreensaver.SetActive(false);
        questions[questionIndex].SetActive(true);
    }

    /// <summary>
    /// Deactivates all question panels
    /// </summary>
    void DeactivateAllQuestions() {

        audioManager.StopSFX();

#if UNITY_ANDROID
        foreach (GameObject go in questionsOculus)
            go.SetActive(false);
#elif UNITY_WEBGL
        foreach (GameObject go in questionsWebGL)
            go.SetActive(false);
#endif
    }

    /// <summary>
    /// Deactivates all characters
    /// </summary>
    void DeactivateAllCharacters() {
        ppeChar.SetActive(false);
        welderChar.SetActive(false);
        welderDown.SetActive(false);
    }

    /// <summary>
    /// Gets current question in order to hide it and activates interactions, WebGL only
    /// </summary>
    /// <param name="display">True to show, false otherwise</param>
    public void DisplayCurrentQuestion(bool display) {

        if (display)
        {
            questions[currentQuestion].SetActive(true);

            questions[currentQuestion].GetComponent<Animator>().SetBool("shrink", false);
            questions[currentQuestion].GetComponent<Animator>().SetBool("grow", true);

            interactions.CanCameraBeZoomed = false;

#if UNITY_WEBGL
            FindObjectOfType<RigidbodyFirstPersonController>().mouseLook.lockCursor = false;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
#endif
        }
        else {
            questions[currentQuestion].GetComponent<Animator>().SetBool("grow", false);
            questions[currentQuestion].GetComponent<Animator>().SetBool("shrink", true);

            interactions.CanCameraBeZoomed = true;

#if UNITY_WEBGL
            FindObjectOfType<RigidbodyFirstPersonController>().mouseLook.lockCursor = true;

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
#endif
        }
    }

    /// <summary>
    /// Marks answer as correct before switching cameras
    /// </summary>
    /// <param name="answer">Selected answer</param>
    public void SetCorrectAnswer(string answer) 
    {
        correctCount++;
        BlockQuestionOptions("right");
        BlockQuestionOptions("wrong");

        data.AddSelectedAnswer(answer);

        cameraSwitch.SwitchCurrentCamera(1);
    }

    /// <summary>
    /// Marks answer as wrong before switching cameras
    /// </summary>
    /// <param name="answer">Selected answer</param> 
    public void SetWrongAnswer(string answer)
    {
        wrongCount++;
        BlockQuestionOptions("right");
        BlockQuestionOptions("wrong");

        data.AddSelectedAnswer(answer);

        cameraSwitch.SwitchCurrentCamera(1);
    }

    /// <summary>
    /// Marks answer as correct before switching to simulation finish event
    /// </summary>
    /// <param name="answer">Selected answer</param>
    public void SetCorrectLastAnswer(string answer) {

        correctCount++;
        BlockQuestionOptions("lastRight");
        BlockQuestionOptions("lastWrong");

        data.AddSelectedAnswer(answer);
        data.AddAnswerCount(correctCount.ToString(), wrongCount.ToString());

        data.CheckDictionaryContents();

        cameraSwitch.SwitchCurrentCamera(1);
    }

    /// <summary>
    /// Marks answer as wrong before switching to simulation finish event
    /// </summary>
    /// <param name="answer">Selected answer</param>
    public void SetWrongLastAnswer(string answer){

        wrongCount++;
        BlockQuestionOptions("lastRight");
        BlockQuestionOptions("lastWrong");

        data.AddSelectedAnswer(answer);
        data.AddAnswerCount(correctCount.ToString(), wrongCount.ToString());

        data.CheckDictionaryContents();

        cameraSwitch.SwitchCurrentCamera(1);
    }

    /// <summary>
    /// Blocks all question's options after answering
    /// </summary>
    void BlockQuestionOptions(string tag) {
#if UNITY_ANDROID

        GameObject[] options = GameObject.FindGameObjectsWithTag(tag);

        foreach (GameObject go in options)
            go.GetComponent<BoxCollider>().enabled = false;

#elif UNITY_WEBGL
    
        FindObjectOfType<WebGLInteractions>().ActivateHelpPanel(false);

        GameObject[] buttons = GameObject.FindGameObjectsWithTag(tag);

        foreach (GameObject go in buttons)
            go.GetComponent<UnityEngine.UI.Button>().interactable = false;
#endif
    }

    /// <summary>
    /// Highlights selected answer
    /// </summary>
    /// <param name="question">GameObject containing the answer</param>
    public void HighlightQuestion(GameObject question) {
        question.GetComponent<SpriteRenderer>().color = new Color(0.03921569f, 0.1960784f, 0.3372549f, 1.0f);
    }

    /// <summary>
    /// Enables/disables shared items and the possibility of using teleport
    /// </summary>
    /// <param name="activate"></param>
    void EnableSharedItems(bool activate) {
#if UNITY_ANDROID
        sharedItems.SetActive(activate);
#endif
    }

    /// <summary>
    /// Enables/disables map zone
    /// </summary>
    /// <param name="activate"></param>
    public void ActivateMap(bool activate) {
#if UNITY_ANDROID
        map.SetActive(activate);
        EnableSharedItems(!activate);
        questions[currentQuestion].SetActive(!activate);
#endif
    }

    /// <summary>
    /// Finishes simulation
    /// </summary>
    public void FinishSimulation() {
        Debug.Log("Simulation finished");

        DeactivateAllCharacters();
        DeactivateAllQuestions();

#if UNITY_ANDROID
        tabletFinish.SetActive(true);
#endif

        data.CorrectCount = correctCount;
        data.WrongCount = wrongCount;

        audioManager.PlayBGM(false);

#if UNITY_WEBGL
        FindObjectOfType<RigidbodyFirstPersonController>().mouseLook.lockCursor = false;

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
#endif

        cameraSwitch.GoToScoreScene();        
    }
}
