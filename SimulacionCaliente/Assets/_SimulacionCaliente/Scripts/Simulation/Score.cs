using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    [Header("Score colors")]
    [SerializeField] Image resultWebGL;
    [SerializeField] Image resultVR;
    [SerializeField] Sprite bad;
    [SerializeField] Sprite ok;
    [SerializeField] Sprite great;

    [Header("Answer sprites")]
    [SerializeField] Sprite wrongAnswer;
    [SerializeField] Sprite rightAnswer;

    [Header("Score panel")]
    [SerializeField] TextMeshProUGUI[] scoreDataVR;
    [SerializeField] TextMeshProUGUI[] scoreDataWebGL;
    [SerializeField] Image[] scoreVR;
    [SerializeField] Image[] scoreWebGL;
    

    Data data;

    // Start is called before the first frame update
    void Start()
    {
        FillScoreData();
    }

    /// <summary>
    /// Fills score data after loading scene
    /// </summary>
    void FillScoreData() {

        data = FindObjectOfType<Data>();

        float right = Mathf.Round((data.CorrectCount / 6.0f) * 100.0f);

#if UNITY_ANDROID
        Debug.Log("panel activated VR");

        //Change sprite
        if (right <= 40.0f)
            resultVR.sprite = bad;
        else if (right > 40.0f && right <= 80.0f)
            resultVR.sprite = ok;
        else if (right > 80.0f)
            resultVR.sprite = great;

        SetImageForAnswer(data.GetSimulationDataByKey("pregunta1"), scoreVR[0]);
        SetImageForAnswer(data.GetSimulationDataByKey("pregunta2"), scoreVR[1]);
        SetImageForAnswer(data.GetSimulationDataByKey("pregunta3"), scoreVR[2]);
        SetImageForAnswer(data.GetSimulationDataByKey("pregunta4"), scoreVR[3]);
        SetImageForAnswer(data.GetSimulationDataByKey("pregunta5"), scoreVR[4]);
        SetImageForAnswer(data.GetSimulationDataByKey("pregunta6"), scoreVR[5]);

        scoreDataVR[0].text = data.GetSimulationDataByKey("nombre");
        scoreDataVR[1].text = "Respuestas correctas: " + right.ToString() + "%";

#elif UNITY_WEBGL
        Debug.Log("panel activated WebGL");

        //Change sprite
        if (right <= 40.0f)
            resultWebGL.sprite = bad;
        else if (right > 40.0f && right <= 80.0f)
            resultWebGL.sprite = ok;
        else if (right > 80.0f)
            resultWebGL.sprite = great;

        SetImageForAnswer(data.GetSimulationDataByKey("pregunta1"), scoreWebGL[0]);
        SetImageForAnswer(data.GetSimulationDataByKey("pregunta2"), scoreWebGL[1]);
        SetImageForAnswer(data.GetSimulationDataByKey("pregunta3"), scoreWebGL[2]);
        SetImageForAnswer(data.GetSimulationDataByKey("pregunta4"), scoreWebGL[3]);
        SetImageForAnswer(data.GetSimulationDataByKey("pregunta5"), scoreWebGL[4]);
        SetImageForAnswer(data.GetSimulationDataByKey("pregunta6"), scoreWebGL[5]);

        scoreDataWebGL[0].text = data.GetSimulationDataByKey("nombre");
        scoreDataWebGL[1].text = "Respuestas correctas: " + right.ToString() + "%";
        
#endif
    }

    /// <summary>
    /// Sets image according to the answer
    /// </summary>
    /// <param name="answer">Answer coming from simulation</param>
    /// <param name="answerObject">Object where icon is goint to be displayed</param>
    void SetImageForAnswer(string answer, Image answerObject)
    {
        if (answer == "right")
            answerObject.sprite = rightAnswer;
        else if (answer == "wrong")
            answerObject.sprite = wrongAnswer;
    }

    /// <summary>
    /// Returns to main scene
    /// </summary>
    public void ReturnToMain() {
        PlayerPrefs.SetString("hand", "right");
        data.ResetAllData();

        UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
    }
}
