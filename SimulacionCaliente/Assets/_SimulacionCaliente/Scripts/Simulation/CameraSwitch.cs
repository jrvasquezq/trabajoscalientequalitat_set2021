using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraSwitch : MonoBehaviour
{
    [Header("Camera")]
    [SerializeField] Transform[] cameraPositions;
    [SerializeField] Transform cameraVR;
    [SerializeField] Transform cameraWebGL;

    [Header("Defaults VR")]
    [SerializeField] Transform defaultPositionVR;
    [SerializeField] Transform xrRig;

    [Header("Camera variables")]
    Transform mainCamera;
    int cameraAmount;
    int currentCamera;
    int teleportCount; //WebGL only. Counts the amount of teleports before returning to the initial index
    bool endReached;
    CameraFade screenFade;

    [Header("Other variables")]
    [SerializeField] Questions questions;

    // Start is called before the first frame update
    void Start()
    {
        cameraAmount = cameraPositions.Length;
        currentCamera = 0;

        QualitySettings.antiAliasing = 8;
        QualitySettings.anisotropicFiltering = AnisotropicFiltering.Enable;

#if UNITY_ANDROID
        mainCamera = cameraVR;
#elif UNITY_WEBGL
        mainCamera = cameraWebGL;
#endif
        screenFade = FindObjectOfType<CameraFade>();

        ChangeCurrentCamera(currentCamera);
    }

    /// <summary>
    /// Switches current camera to the next one
    /// </summary>
    /// <param name="indexChange">Camera index in array</param>
    public void SwitchCurrentCamera(int indexChange)
    {
        StartCoroutine(ChangeCamera(indexChange));
    }

    /// <summary>
    /// Changes current camera to the next one
    /// </summary>
    /// <param name="index">Amount of units the index will be increased</param>
    void ChangeCurrentCamera(int index)
    {

        currentCamera = currentCamera + index;

        if (currentCamera < 0)
            currentCamera = cameraAmount - 1;
        else if (currentCamera == cameraAmount)
        {
            currentCamera = 0;
            endReached = true;
        }

        mainCamera.localPosition = cameraPositions[currentCamera].localPosition;
        mainCamera.localRotation = cameraPositions[currentCamera].localRotation;

#if UNITY_ANDROID
        xrRig.localPosition = defaultPositionVR.localPosition;
        xrRig.localRotation = defaultPositionVR.localRotation;
#endif

        if (!SceneManager.GetActiveScene().name.Contains("Tutorial"))
        {
            if (!endReached)
                questions.ActivateQuestion(currentCamera);
            else if (endReached)
                questions.FinishSimulation();
        }
    }

    /// <summary>
    /// Changes current camera to a particular one without changing the question
    /// </summary>
    /// <param name="index"></param>
    public void ChangeCurrentLocation(int index)
    {
#if UNITY_ANDROID
        StartCoroutine(ChangeLocation(index));
#elif UNITY_WEBGL

        teleportCount = teleportCount + index;

        if (teleportCount == cameraAmount)
            teleportCount = 0;

        StartCoroutine(ChangeLocation(teleportCount));
#endif
    }

    IEnumerator ChangeLocation(int index)
    {
        screenFade.FadeOut();
        yield return new WaitForSeconds(2.5f);

        mainCamera.localPosition = cameraPositions[index].localPosition;
        mainCamera.localRotation = cameraPositions[index].localRotation;

#if UNITY_ANDROID
        xrRig.localPosition = defaultPositionVR.localPosition;
        xrRig.localRotation = defaultPositionVR.localRotation;
#endif
        yield return new WaitForSeconds(1.0f);

        yield return new WaitForEndOfFrame();
        screenFade.FadeIn();

#if UNITY_WEBGL
        FindObjectOfType<WebGLInteractions>().TeleportEnabled = true;

        if (SceneManager.GetActiveScene().name == "Tutorial")
            FindObjectOfType<TutorialWeb>().IncreaseTeleportCounter();

#endif
    }

    IEnumerator ChangeCamera(int indexChange)
    {

        screenFade.FadeOut();
        yield return new WaitForSeconds(4.0f);

        ChangeCurrentCamera(indexChange);

        yield return new WaitForSeconds(1.0f);

        yield return new WaitForEndOfFrame();

        if (!endReached)
            screenFade.FadeIn();            
    }

    /// <summary>
    /// Returns to Main scene
    /// </summary>
    public void ReturnToMain()
    {
        SceneManager.LoadScene("Main");
    }

    /// <summary>
    /// Redirect to score scene
    /// </summary>
    public void GoToScoreScene()
    {
        StartCoroutine(ScoreScene());
    }

    IEnumerator ScoreScene()
    {
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("Score");
    }

    public void GoToTargetScene(string scene)
    {
        StartCoroutine(TargetScene(scene));
    }

    IEnumerator TargetScene(string target)
    {
        screenFade.FadeOut();
        yield return new WaitForSeconds(4.0f);

        SceneManager.LoadScene(target);
    }
}
