using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HandCollision : MonoBehaviour
{
    Questions questions;
    CameraSwitch cameraSwitch;

    bool tutorialAnswer;
    bool simulationAnswer;
    bool answerSelected;
    float fillAnswerSpeed; //VR ONLY
    Transform answerFill;

    AnswerType answerType;

    public bool AnswerSelected { get => answerSelected; set => answerSelected = value; }

    private void Start()
    {
        questions = FindObjectOfType<Questions>();
        cameraSwitch = FindObjectOfType<CameraSwitch>();

        fillAnswerSpeed = 0.01f;
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (other.tag) {
            case "right":
                answerFill = other.transform.GetChild(1);
                simulationAnswer = true;
                answerType = AnswerType.right;
                break;

            case "wrong":
                answerFill = other.transform.GetChild(1);
                simulationAnswer = true;
                answerType = AnswerType.wrong;
                break;

            case "lastRight":
                answerFill = other.transform.GetChild(1);
                simulationAnswer = true;
                answerType = AnswerType.lastRight;
                break;

            case "lastWrong":
                answerFill = other.transform.GetChild(1);
                simulationAnswer = true;
                answerType = AnswerType.lastWrong;;
                break;

            /* LOCATION CHANGE */
            case "location":
                cameraSwitch.ChangeCurrentLocation(0);

                if (SceneManager.GetActiveScene().name == "Tutorial" && FindObjectOfType<TutorialVR>().GetCurrentLesson() == 1)
                    FindObjectOfType<TutorialVR>().IncreaseCounterVRTutorial();
                break;

            case "locationB":
                cameraSwitch.ChangeCurrentLocation(1);

                if (SceneManager.GetActiveScene().name == "Tutorial" && FindObjectOfType<TutorialVR>().GetCurrentLesson() == 1)
                    FindObjectOfType<TutorialVR>().IncreaseCounterVRTutorial();
                break;

            case "locationC":
                cameraSwitch.ChangeCurrentLocation(2);

                if (SceneManager.GetActiveScene().name == "Tutorial" && FindObjectOfType<TutorialVR>().GetCurrentLesson() == 1)
                    FindObjectOfType<TutorialVR>().IncreaseCounterVRTutorial();
                break;

            case "locationD":
                cameraSwitch.ChangeCurrentLocation(3);

                if (SceneManager.GetActiveScene().name == "Tutorial" && FindObjectOfType<TutorialVR>().GetCurrentLesson() == 1)
                    FindObjectOfType<TutorialVR>().IncreaseCounterVRTutorial();
                break;

            case "locationE":
                cameraSwitch.ChangeCurrentLocation(4);

                if (SceneManager.GetActiveScene().name == "Tutorial" && FindObjectOfType<TutorialVR>().GetCurrentLesson() == 1)
                    FindObjectOfType<TutorialVR>().IncreaseCounterVRTutorial();
                break;

            case "close":
                if (SceneManager.GetActiveScene().name == "Tutorial")
                    FindObjectOfType<TutorialVR>().ActivateMap(false);
                else
                    FindObjectOfType<Questions>().ActivateMap(false);
                break;

            case "openLocation":
                if (SceneManager.GetActiveScene().name == "Tutorial")
                    FindObjectOfType<TutorialVR>().ActivateMap(true);
                else
                    FindObjectOfType<Questions>().ActivateMap(true);
                break;
            /* END LOCATION CHANGE */

            /*TUTORIAL*/
            case "answer":
                if (!tutorialAnswer)
                {
                    tutorialAnswer = true;
                    answerFill = other.transform.GetChild(1);
                }
                break;
            /* END TUTORIAL */
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //Colliding with answers
        if (SceneManager.GetActiveScene().name == "Tutorial")
        {
            if (tutorialAnswer) {
                if (answerFill.localScale.x < 1.0f)
                    answerFill.localScale = new Vector3(answerFill.localScale.x + fillAnswerSpeed, 1.0f, 1.0f);
                else if (answerFill.localScale.x >= 1.0f)
                    AnswerSelected = true;
            }

            if (AnswerSelected) {
                Debug.Log("Adding counter to VR Tutorial");
                other.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.03921569f, 0.1960784f, 0.3372549f, 1.0f);
                FindObjectOfType<TutorialVR>().DisableQuestionColliders();
                FindObjectOfType<TutorialVR>().IncreaseCounterVRTutorial();
                AnswerSelected = false;
            }
        }
        else {
            if (simulationAnswer) 
            {
                if (answerFill.localScale.x < 1.0f)
                    answerFill.localScale = new Vector3(answerFill.localScale.x + fillAnswerSpeed, 1.0f, 1.0f);
                else if (answerFill.localScale.x >= 1.0f)
                    AnswerSelected = true;
            }

            if (AnswerSelected) {
                Debug.Log("Answering question VR Simulation");
                AnswerSelected = false;
                other.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.03921569f, 0.1960784f, 0.3372549f, 1.0f);

                switch(answerType){
                    case AnswerType.right:
                        questions.SetCorrectAnswer(other.gameObject.name);
                        break;

                    case AnswerType.wrong:
                        questions.SetWrongAnswer(other.gameObject.name);
                        break;

                    case AnswerType.lastRight:
                        questions.SetCorrectLastAnswer(other.gameObject.name);
                        break;

                    case AnswerType.lastWrong:
                        questions.SetWrongLastAnswer(other.gameObject.name);
                        break;

                }                
            }
        }        
    }

    private void OnTriggerExit(Collider other)
    {
        if (SceneManager.GetActiveScene().name == "Tutorial")
            ResetTutorialVariables();
        else
            ResetSimulationVariables();

        ResetFill();
    }

    /// <summary>
    /// Resets answer fill
    /// </summary>
    private void ResetFill() {

        GameObject[] ansFill = GameObject.FindGameObjectsWithTag("answerFill");

        foreach (GameObject go in ansFill)
        {
            go.transform.localScale = new Vector3(0.0f, 1.0f, 1.0f);
        }
        
        answerFill = null;
        answerSelected = false;        
    }

    /// <summary>
    /// Resets tutorial variables
    /// </summary>
    private void ResetTutorialVariables() {
        tutorialAnswer = false;
    }

    /// <summary>
    /// Resets collider variables before answering a new question
    /// </summary>
    public void ResetSimulationVariables()
    {
        simulationAnswer = false;
        answerType = AnswerType.none;
    }
}

enum AnswerType { 
    none,
    right,
    wrong,
    lastRight,
    lastWrong
}