using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BodyCollision : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Indicator circle
        if (other.tag == "circle") {
            if (SceneManager.GetActiveScene().name == "Tutorial")
            {
                other.GetComponent<Collider>().enabled = false;
                FindObjectOfType<TutorialWeb>().IncreaseArrowCounter(other.transform.parent.gameObject);
            }
            else {
                other.transform.parent.gameObject.SetActive(false);
                FindObjectOfType<Questions>().DisplayCurrentQuestion(true);
            }
            
        }
    }
}
