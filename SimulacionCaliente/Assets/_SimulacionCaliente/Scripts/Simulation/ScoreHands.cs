using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreHands : MonoBehaviour
{
    [Header("Hands")]
    [SerializeField] GameObject leftHandController;
    [SerializeField] GameObject rightHandController;

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        ChangeDominantHand(PlayerPrefs.GetString("hand"));
#endif
    }

    
    /// <summary>
    /// Changes current dominant hand
    /// </summary>
    /// <param name="hand"></param>
    public void ChangeDominantHand(string hand)
    {
        switch (hand)
        {
            case "izq":
                rightHandController.SetActive(false);
                leftHandController.SetActive(true);
                break;

            case "der":
                leftHandController.SetActive(false);
                rightHandController.SetActive(true);
                break;
        }
    }
}
