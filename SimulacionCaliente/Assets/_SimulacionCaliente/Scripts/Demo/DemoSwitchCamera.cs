using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoSwitchCamera : MonoBehaviour
{
    CameraSwitch cameraSwitch;
    [SerializeField] Transform mainCamera;
    [SerializeField] GameObject postProcessing;

    // Start is called before the first frame update
    void Start()
    {
        cameraSwitch = FindObjectOfType<CameraSwitch>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckInput();
        //RotateCamera();
    }

    private void RotateCamera()
    {
        if (Input.GetKey(KeyCode.A))
            mainCamera.Rotate(0, -10.0f * Time.deltaTime, 0);
        if (Input.GetKey(KeyCode.D))
            mainCamera.Rotate(0, 10.0f * Time.deltaTime, 0);
    }

    public void Exit() {
        Application.Quit();
    }

    void CheckInput() {
        
        /***********QUESTIONS**********/
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            GameObject.Find("AnswerA").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            GameObject.Find("AnswerB").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            GameObject.Find("AnswerC").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            GameObject.Find("End").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
        }
    }
}
