using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FPS : MonoBehaviour
{
    [SerializeField] TextMeshPro textMeshPro;
    int myFps = 0;
    float timer;

    // Start is called before the first frame update
    void Start()
    {

    }

    void Update()
    {
        myFps = (int)(1f / Time.unscaledDeltaTime);               
    }

    void FixedUpdate() {

        timer = timer + Time.fixedDeltaTime;

        if (timer > 2.0f)
        {
            timer = 0.0f;
            textMeshPro.text = myFps.ToString();
        }
    }
}