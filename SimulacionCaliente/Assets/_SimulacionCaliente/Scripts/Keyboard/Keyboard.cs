using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Keyboard : MonoBehaviour
{
    [Header("Oculus")]
    [SerializeField] TMP_InputField document;
    [SerializeField] TMP_InputField nameField;

    string documentString;
    string nameString;

    public string DocumentString { get => documentString; set => documentString = value; }
    public string NameString { get => nameString; set => nameString = value; }


    /// <summary>
    /// Clears input
    /// </summary>
    /// <param name="keyboard">Current keyboard</param>
    public void Clear(string keyboard) {
        if (keyboard == "name")
        {
            NameString = "";
            nameField.text = "";
        }
        else if (keyboard == "id")
        {
            documentString = "";
            document.text = "";
        }
    }

    public void EraseLetter() {
        string temp = NameString;

        if (temp != "")
        {
            NameString = temp.Substring(0, temp.Length - 1);
            nameField.text = NameString;
        }
    }

    public void EraseNumber() {

        string temp = DocumentString;

        if (temp != "")
        {
            DocumentString = temp.Substring(0, temp.Length - 1);
            document.text = DocumentString;
        }

        document.text = DocumentString;
    }

    public void AddLetter(GameObject go) {
        NameString = NameString + go.name;
        nameField.text = NameString;
    }

    public void AddNumber(GameObject go) {

        if (document.text.Length < 9)
        {
            DocumentString = DocumentString + go.name;
            document.text = DocumentString;
        }
    }
}
