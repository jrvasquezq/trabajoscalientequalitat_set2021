using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{
    Dictionary<string, string> simulationData;
    int questionIndex;
    int correctCount;
    int wrongCount;

    public int CorrectCount { get => correctCount; set => correctCount = value; }
    public int WrongCount { get => wrongCount; set => wrongCount = value; }

    // Start is called before the first frame update
    void Start()
    {
        simulationData = new Dictionary<string, string>();
        questionIndex = 0;
    }

    /// <summary>
    /// Resets all dictionary data. This should be called before returning to Main scene
    /// </summary>
    public void ResetAllData(){
        simulationData.Clear();
    }

    /// <summary>
    /// Adds user ID to simulation data dictionary
    /// </summary>
    /// <param name="user">Current user</param>
    /// <param name="name">Current user name</param>
    public void AddUserId(string user, string name) {
        simulationData.Add("id", user);
        simulationData.Add("nombre", name);
    }

    /// <summary>
    /// Adds selected answer to simulation data dictionary
    /// </summary>
    /// <param name="answer">Selected answer letter</param>
    public void AddSelectedAnswer(string answer) {
        switch (questionIndex) {
            case 0:
                simulationData.Add("pregunta1", answer);
                break;

            case 1:
                simulationData.Add("pregunta2", answer);
                break;

            case 2:
                simulationData.Add("pregunta3", answer);
                break;

            case 3:
                simulationData.Add("pregunta4", answer);
                break;

            case 4:
                simulationData.Add("pregunta5", answer);
                break;

            case 5:
                simulationData.Add("pregunta6", answer);
                break;
        }

        questionIndex++;
    }

    /// <summary>
    /// Adds answer count to simulation data dictionary
    /// </summary>
    /// <param name="correct">Correct answers amount</param>
    /// <param name="wrong">Incorrect answers amount</param>
    public void AddAnswerCount(string correct, string wrong) {
        simulationData.Add("correctas", correct);
        simulationData.Add("incorrectas", wrong);
    }

    /// <summary>
    /// Debug only. Checks if all the information has been stored correctly
    /// </summary>
    public void CheckDictionaryContents() {
#if UNITY_EDITOR
        Debug.LogFormat("{0},{1},{2},{3},{4},{5},{6}",
            simulationData["pregunta1"], 
            simulationData["pregunta2"], 
            simulationData["pregunta3"], 
            simulationData["pregunta4"], 
            simulationData["pregunta5"],
            simulationData["correctas"],
            simulationData["incorrectas"]);
#endif
    }

    /// <summary>
    /// Gets simulation data. Keys return the selected answer letter.
    /// The current dictionary keys are:
    /// id (user's id number), nombre,
    /// pregunta1, pregunta2, pregunta3, pregunta4, pregunta5, correctas, incorrectas
    /// </summary>
    /// <returns>A dictionary with all the current run data</returns>
    public Dictionary<string, string> GetSimulationData()
    {
        return simulationData;
    }

    /// <summary>
    /// Gets simulation data by key
    /// </summary>
    /// <param name="key">Data key</param>
    /// <returns>Value from given key</returns>
    public string GetSimulationDataByKey(string key) {

        string value = "";

        try
        {
            value = simulationData[key];
        }
        catch (System.Exception ex) {
            Debug.Log("Exception: " + ex.Message);
            value = "";
        }

        return value;
    }
}
